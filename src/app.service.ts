import { Injectable } from '@nestjs/common';
const { name, description, version } = require('../package.json');

@Injectable()
export class AppService {
  healthCheck() {
    return {
      name,
      description,
      version
    }
  }
}
