import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as helmet from 'helmet';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationFilter } from './common/filters/validation.filter';
import { ValidationPipe, ValidationError } from '@nestjs/common';
import { ValidationException } from './common/exceptions/validation.exception';
import 'dotenv/config'
const { name, description, version } = require('../package.json');

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.set('trust proxy', 1)
  app.enableCors()
  app.use(helmet());
  app.setGlobalPrefix('api')

  app.useGlobalFilters(
    new ValidationFilter()
  )
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    skipMissingProperties: true,
    exceptionFactory: (errors: ValidationError[]) => {
      //console.log(errors)
      return new ValidationException(errors)
    }
  }))

  if(process.env.ENV == 'DEV') {
    const swaggerOptions = new DocumentBuilder()
    .setTitle(name)
    .setDescription(description)
    .setVersion(version)
    .addBearerAuth()
    .build();
    const document = SwaggerModule.createDocument(app, swaggerOptions);
    SwaggerModule.setup('api/docs', app, document);
  }
  
  await app.listen(process.env.APP_PORT);
}
bootstrap();
