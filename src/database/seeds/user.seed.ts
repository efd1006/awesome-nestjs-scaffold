import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm'
import { hashPassword } from '../../common/helpers'

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {

    let password = 'password'

    let users = [
      {
        email: 'user1@mail.com',
        password: await hashPassword(password)
      }
    ]

    await connection
      .createQueryBuilder()
      .insert()
      .into('users')
      .values(users)
      .execute()
  }
}