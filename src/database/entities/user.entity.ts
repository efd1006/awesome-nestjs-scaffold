import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, BeforeInsert } from "typeorm";
import { BaseEntity } from "./base.entity";
import { v4 as uuidv4 } from 'uuid';
import { hashPassword } from "../../common/helpers";

@Entity('users')
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    type: 'varchar',
    length: 255
  })
  email: string

  @Column({
    type: 'varchar',
    length: 255
  })
  password: string

  @BeforeInsert()
  async performBeforeInsert() {
    this.password = await hashPassword(this.password)
  }
}