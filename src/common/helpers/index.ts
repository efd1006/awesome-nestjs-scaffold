import * as bcrypt from 'bcryptjs'
import 'dotenv/config'

export async function hashPassword(password: string) {
  return await bcrypt.hash(password, Number(process.env.PASSWORD_SALT))
}